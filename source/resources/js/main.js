// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {
  
  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 
      
    },
    finalize: function() {
      
    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 
      
    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here
 
    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;
 
    // hit up common first.
    UTIL.fire( 'common' );
 
    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);


(function () {
    var link, num_pos, pin_code, press;
    num_pos = 1;
    pin_code = '';
    $('#numpad li a').tap(function (event) {
        var target;
        target = $(event.target);
        target.addClass('pressed');
        setTimeout(function () {
            return target.removeClass('pressed');
        }, 500);
        return press(target.data("value"));
    });

    press = function (command) {
        switch (command) {

        default:
            $('#login li:nth-child(' + num_pos + ') input').val(command).addClass('active');
            num_pos++;
            if (num_pos > 4) {
                return num_pos = 1;
            }
        }
    };
    $('.clear').click(function(){
      $('#login li input').removeClass('active').val('');
    })
}.call(this));




/*
  $(".login .container-fluid .row")
    .velocity("transition.flipBounceYIn", {duration:1500, stagger: 200 })
    .delay(1500)
*/

var transition = 'transition.slideLeftIn';

$.Velocity.RunSequence([
    { e: $('.logo'), p: 'fadeIn', o: {duration: 350} },
    { e: $('.info'), p: 'fadeIn', o: {delay:500, duration: 350} },
    { e: $('#login input'), p: transition, o: { duration: 250, stagger: 100} },
    { e: $('#numpad li'), p: transition, o: { duration: 150, stagger:100, display:'inline-block'} },
]);
